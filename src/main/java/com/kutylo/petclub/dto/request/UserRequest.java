package com.kutylo.petclub.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    @NotNull(message = "User must not be null")
    private String username;
    @NotNull
    private String password;
    @NotNull
    @Email(message = "Invalid email")
    private String email;
    @NotNull
    @Pattern(regexp = "^([+]380){1}\\d{9}$", message = "Invalid phone number")
    private String phone;
    private String imgUrl;
}
