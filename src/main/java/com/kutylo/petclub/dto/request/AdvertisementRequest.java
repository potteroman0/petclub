package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdvertisementRequest {
    private String title;
    private String description;
    private UserResponse user;
}
