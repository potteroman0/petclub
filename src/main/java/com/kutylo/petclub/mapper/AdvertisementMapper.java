package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.AdvertisementRequest;
import com.kutylo.petclub.dto.response.AdvertisementResponse;
import com.kutylo.petclub.model.Advertisement;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AdvertisementMapper {

    AdvertisementResponse modelToResponse(Advertisement advertisement);

    Advertisement requestToModel(AdvertisementRequest advertisementRequest);
}
