package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.CommentRequest;
import com.kutylo.petclub.dto.response.CommentResponse;
import com.kutylo.petclub.model.Comment;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    Comment requestToModel(CommentRequest commentRequest);

    CommentResponse modelToResponse(Comment comment);
}
