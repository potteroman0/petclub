package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.UserRequest;
import com.kutylo.petclub.dto.response.UserResponse;
import com.kutylo.petclub.feign.ClusteringClient;
import com.kutylo.petclub.mapper.UserMapper;
import com.kutylo.petclub.model.User;
import com.kutylo.petclub.repository.UserRepository;
import com.kutylo.petclub.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private BCryptPasswordEncoder encoder;
    @Autowired
    private ClusteringClient clusteringClient;

    @PostConstruct
    private void initEncoder() {
        encoder = new BCryptPasswordEncoder();
    }

    public String getUserCluster(String username) {
        return clusteringClient.getUserCluster(username);
    }

    @Override
    public UserResponse getCurrentUser() {
        log.info("Get current user");
        User user = getCurrentByToken();
        return userMapper.modelToResponse(user);
    }

    @Override
    public List<UserResponse> getAllUsers() {
        log.info("Get all users");
        return userRepository.findAll()
                .stream()
                .map(userMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse getUserById(int id) {
        log.info("Get user by id:{}", id);
        User user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found user with id: " + id));
        return userMapper.modelToResponse(user);
    }

    @Override
    public UserResponse getUserByUsername(String username) {
        log.info("Get user by username:{}", username);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("Not found user with id: " + username));
        return userMapper.modelToResponse(user);
    }

    @Override
    public UserResponse save(UserRequest userRequest) {
        log.info("Save user:{}", userRequest);
        User user = userMapper.requestToModel(userRequest);
        if (!isUsernameValid(user.getUsername())) {
            throw new IllegalArgumentException("Username should be unique");
        } else if (!checkIfEmailIsValid(user.getEmail())) {
            throw new IllegalArgumentException("Email should be unique");
        }
        user.setPassword(encoder.encode(userRequest.getPassword()));
        userRepository.save(user);
        return userMapper.modelToResponse(user);
    }

    @Override
    public UserResponse update(int id, UserRequest userRequest) {
        log.info("Update user with id:{}, user:{}", id, userRequest);
        User user = userMapper.requestToModel(userRequest);
        if (!isUsernameValid(user.getUsername())) {
            throw new IllegalArgumentException("Username should be unique");
        } else if (!checkIfEmailIsValid(user.getEmail())) {
            throw new IllegalArgumentException("Email should be unique");
        }
        user.setId(id);
        user.setPassword(encoder.encode(userRequest.getPassword()));
        userRepository.save(user);
        return userMapper.modelToResponse(user);
    }

    @Override
    public void delete(int id) {
        log.info("Delete user with id:{}", id);
        userRepository.deleteById(id);
    }

    private boolean isUsernameValid(String username) {
        return userRepository.findByUsername(username).isEmpty()
                || getCurrentByToken().getUsername().equals(username);
    }

    private boolean checkIfEmailIsValid(String email) {
        return userRepository.findByEmail(email).isEmpty() ||
                getCurrentByToken().getEmail().equals(email);
    }

    private User getCurrentByToken() {
        log.info("Get current user by token");
        String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        if (currentUsername.equalsIgnoreCase("anonymousUser")) return User.builder().username("anon").build();
        return userRepository.findByUsername(currentUsername)
                .orElseThrow(() -> new EntityNotFoundException("Not found user"));
    }
}
