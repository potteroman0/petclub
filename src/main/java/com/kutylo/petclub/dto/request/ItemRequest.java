package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.model.ItemCategory;
import com.kutylo.petclub.model.PetType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemRequest {
    private String title;
    private String description;
    @NotNull
    private int amount;
    @NotNull
    private double price;
    private String imgUrl;
    private ItemCategory type;
    private PetType petType;
}
