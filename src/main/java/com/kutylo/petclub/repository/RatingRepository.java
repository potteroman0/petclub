package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {
    Optional<List<Rating>> findRatingsByArticleId(int id);

    Optional<List<Rating>> findRatingsByUserId(int id);
}
