package com.kutylo.petclub.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class PageResponse<T> {
    private Integer totalPages;
    private Long totalElements;
    private List<T> data = new ArrayList<>();

}
