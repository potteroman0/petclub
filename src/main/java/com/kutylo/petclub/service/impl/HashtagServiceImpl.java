package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.HashtagRequest;
import com.kutylo.petclub.dto.response.HashtagResponse;
import com.kutylo.petclub.mapper.HashtagMapper;
import com.kutylo.petclub.model.Hashtag;
import com.kutylo.petclub.repository.HashtagRepository;
import com.kutylo.petclub.service.HashtagService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class HashtagServiceImpl implements HashtagService {

    private final HashtagRepository hashtagRepository;
    private final HashtagMapper hashtagMapper;

    @Override
    public List<HashtagResponse> getAllHashtags() {
        log.info("Get all hashtags");

        return hashtagRepository.findAll()
                .stream()
                .filter(Objects::nonNull)
                .map(hashtagMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public HashtagResponse getById(int id) {
        log.info("Get hashtag by id:{}", id);

        Hashtag hashtag = hashtagRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found entity with id: " + id));
        return hashtagMapper.modelToResponse(hashtag);
    }

    @Override
    public HashtagResponse save(HashtagRequest hashtagRequest) {
        log.info("Save hashtag:{}", hashtagRequest);

        Hashtag hashTag = hashtagMapper.requestToModel(hashtagRequest);
        hashtagRepository.save(hashTag);
        return hashtagMapper.modelToResponse(hashTag);
    }

    @Override
    public void delete(int id) {
        log.info("Delete hashtag with id:{}", id);
        hashtagRepository.deleteById(id);
    }

    @Override
    public Hashtag checkIfExists(HashtagRequest hashtag) {
        log.info("Check if hashtag exist:{}", hashtag);
        return findByName(hashtag.getName());
    }

    private Hashtag findByName(String name) {
        return hashtagRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Not found hashtag with name: " + name));
    }
}
