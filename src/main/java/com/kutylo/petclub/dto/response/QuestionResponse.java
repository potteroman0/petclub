package com.kutylo.petclub.dto.response;

import com.kutylo.petclub.model.CategoryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {
    private int id;
    private String title;
    private String text;
    private String imgUrl;
    private LocalDateTime createDate;
    private CategoryType category;
    private boolean isResolved;
    private UserResponse user;
    private ArrayList<QuestionAnswerResponse> questionAnswers;
}
