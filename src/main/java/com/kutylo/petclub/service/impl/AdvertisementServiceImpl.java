package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.AdvertisementRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.AdvertisementResponse;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.mapper.AdvertisementMapper;
import com.kutylo.petclub.model.Advertisement;
import com.kutylo.petclub.repository.AdvertisementRepository;
import com.kutylo.petclub.service.AdvertisementService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdvertisementServiceImpl implements AdvertisementService {

    private final AdvertisementMapper advertisementMapper;
    private final AdvertisementRepository advertisementRepository;

    @Override
    public PageResponse<AdvertisementResponse> getAllAdvertisements(PaginationRequest paginationRequest) {
        Pageable pageable = paginationRequest.toPageable();
        Page<Advertisement> advertisements = advertisementRepository.findAll(pageable);
        PageResponse<AdvertisementResponse> response = new PageResponse<AdvertisementResponse>(
                advertisements.getTotalPages(),
                advertisements.getTotalElements(),
                advertisements.map(advertisementMapper::modelToResponse)
                        .get().collect(Collectors.toList())
        );
        return response;
    }

    @Override
    public AdvertisementResponse getAdvertisementById(int id) {
        Advertisement advertisement = advertisementRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException());
        return advertisementMapper.modelToResponse(advertisement);
    }

    @Override
    public AdvertisementResponse save(AdvertisementRequest advertisementRequest) {
        Advertisement advertisement = advertisementMapper.requestToModel(advertisementRequest);
        advertisement.setCreateDate(LocalDateTime.now());
        advertisementRepository.save(advertisement);

        return advertisementMapper.modelToResponse(advertisement);
    }

    @Override
    public AdvertisementResponse update(int id, AdvertisementRequest advertisementRequest) {
        Advertisement advertisement = advertisementMapper.requestToModel(advertisementRequest);
        advertisement.setId(id);
        advertisement.setCreateDate(LocalDateTime.now());
        advertisementRepository.save(advertisement);

        return advertisementMapper.modelToResponse(advertisement);
    }

    @Override
    public void delete(int id) {
        advertisementRepository.deleteById(id);
    }
}
