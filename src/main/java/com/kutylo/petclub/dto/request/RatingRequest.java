package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RatingRequest {
    private double rate;
    private UserResponse user;
    private ArticleResponse article;
}
