package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.PetRequest;
import com.kutylo.petclub.dto.response.PetResponse;
import com.kutylo.petclub.mapper.PetMapper;
import com.kutylo.petclub.model.Pet;
import com.kutylo.petclub.repository.PetRepository;
import com.kutylo.petclub.service.PetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PetServiceImpl implements PetService {

    private final PetRepository petRepository;
    private final PetMapper petMapper;

    @Override
    public List<PetResponse> getPetsByUserId(int id) {
        log.info("Get user pets for user with id:{}", id);
        return petRepository.findPetsByUserId(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found entity for user with id: " + id))
                .stream()
                .map(petMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public PetResponse getPetById(int id) {
        log.info("Get pet by id:{}", id);
        Pet pet = petRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found entity with id: " + id));
        return petMapper.modelToResponse(pet);
    }

    @Override
    public PetResponse save(PetRequest petRequest) {
        log.info("Save pet:{}", petRequest);
        Pet pet = petMapper.requestToModel(petRequest);
        petRepository.save(pet);
        return petMapper.modelToResponse(pet);
    }

    @Override
    public PetResponse update(int id, PetRequest petRequest) {
        log.info("Update pet with id:{}, pet:{}", id, petRequest);
        Pet pet = petMapper.requestToModel(petRequest);
        pet.setId(id);
        petRepository.save(pet);
        return petMapper.modelToResponse(pet);
    }

    @Override
    public void delete(int id) {
        log.info("Delete pet with id:{}", id);
        petRepository.deleteById(id);
    }
}
