package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.AuthenticationRequest;
import com.kutylo.petclub.dto.response.AuthenticationResponse;

public interface AuthenticationService {
    AuthenticationResponse login(AuthenticationRequest authenticationRequest);
}
