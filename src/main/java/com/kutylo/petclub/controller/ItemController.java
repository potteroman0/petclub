package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.ItemRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.ItemResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.service.ItemService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/shop/items")
public class ItemController {

    private final ItemService itemService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, ItemResponse> getItemById(@PathVariable int id) {
        return Map.of("item", itemService.getItemById(id));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, PageResponse<ItemResponse>> getAllItems(@Validated PaginationRequest paginationRequest) {
        return Map.of("items", itemService.getAllItems(paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/category/{category}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, PageResponse<ItemResponse>> getAllItemsByCategory(@PathVariable String category, @Validated PaginationRequest paginationRequest) {
        return Map.of("items", itemService.getAllItemsByCategory(category, paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/pet/{petType}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, PageResponse<ItemResponse>> getAllItemsByPetType(@PathVariable String petType, @Validated PaginationRequest paginationRequest) {
        return Map.of("items", itemService.getAllItemsByPetType(petType, paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/categories",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, List<String>> getAllItemCategory() {
        return Map.of("categories", itemService.getAllItemCategory());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/petTypes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found")
    })
    Map<String, List<String>> getAllItemPetTypes() {
        return Map.of("types", itemService.getAllItemPetType());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Item created", response = ItemResponse.class)
    })
    Map<String, ItemResponse> save(@RequestBody @Validated ItemRequest itemRequest) {
        return Map.of("item", itemService.save(itemRequest));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Article created", response = ArticleResponse.class)
    })
    Map<String, ItemResponse> update(@PathVariable int id, @RequestBody ItemRequest itemRequest) {
        return Map.of("item", itemService.update(id, itemRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Item deleted")
    })
    void delete(@PathVariable int id) {
        itemService.delete(id);
    }

}
