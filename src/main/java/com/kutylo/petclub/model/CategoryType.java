package com.kutylo.petclub.model;

public enum CategoryType {
    HEALTH("health"), CARE("care"), EAT("eat");

    private String name;

    CategoryType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
