package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.ArticleRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.RatingRequest;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.mapper.ArticleMapper;
import com.kutylo.petclub.model.Article;
import com.kutylo.petclub.model.Hashtag;
import com.kutylo.petclub.repository.ArticleRepository;
import com.kutylo.petclub.service.ArticleService;
import com.kutylo.petclub.service.HashtagService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;
    private final ArticleMapper articleMapper;
    private final HashtagService hashtagService;
    private final RatingServiceImpl ratingService;

    @Override
    public ArticleResponse getArticleById(int id) {
        log.info("Get article by id:{}", id);
        Article article = articleRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Not found article with id:" + id));
        ArticleResponse response = articleMapper.modelToResponse(article);
        response.setRating(ratingService.getMeanRatingByArticleId(id));
        return response;
    }

    @Override
    public PageResponse<ArticleResponse> getAllArticles(PaginationRequest paginationRequest) {
        log.info("Get all articles");
        Pageable pageable = paginationRequest.toPageable();
        Page<Article> articles = articleRepository.findAll(pageable);
        PageResponse<ArticleResponse> pageResponse = new PageResponse<ArticleResponse>(
                articles.getTotalPages(),
                articles.getTotalElements(),
                articles.map(articleMapper::modelToResponse)
                        .get()
                        .peek(articleResponse -> articleResponse.setRating(ratingService.getMeanRatingByArticleId(articleResponse.getId())))
                        .collect(Collectors.toList())
        );

        return pageResponse;
    }

    @Override
    public PageResponse<ArticleResponse> getAllArticlesByHashtag(String hashtag, PaginationRequest paginationRequest) {
        log.info("Get articles by hashtag:{}", hashtag);

        Pageable pageable = paginationRequest.toPageable();
        Page<Article> articles = articleRepository.findArticlesByHashtagsName(hashtag, pageable);

        return new PageResponse<ArticleResponse>(
                articles.getTotalPages(),
                articles.getTotalElements(),
                articles.map(articleMapper::modelToResponse)
                        .get()
                        .peek(articleResponse -> articleResponse.setRating(ratingService.getMeanRatingByArticleId(articleResponse.getId())))
                        .collect(Collectors.toList())
        );
    }

    @Override
    public PageResponse<ArticleResponse> getAllArticleByUserId(int id, PaginationRequest paginationRequest) {
        log.info("Get articles by user id:{}", id);

        Pageable pageable = paginationRequest.toPageable();
        Page<Article> articles = articleRepository.findArticlesByUserId(id, pageable);

        return new PageResponse<ArticleResponse>(
                articles.getTotalPages(),
                articles.getTotalElements(),
                articles.map(articleMapper::modelToResponse)
                        .get()
                        .peek(articleResponse -> articleResponse.setRating(ratingService.getMeanRatingByArticleId(articleResponse.getId())))
                        .collect(Collectors.toList())
        );
    }

    @Override
    public ArticleResponse save(ArticleRequest articleRequest) {
        log.info("Create article: {}", articleRequest);

        Article article = articleMapper.requestToModel(articleRequest);
        List<Hashtag> hashtags = articleRequest.getHashtags()
                .stream()
                .map(hashtagService::checkIfExists)
                .collect(Collectors.toList());
        article.setHashtags(hashtags);
        article.setCreateDate(LocalDateTime.now());
        article.setRating(0.0);

        articleRepository.save(article);
        return articleMapper.modelToResponse(article);
    }

    @Override
    public ArticleResponse update(int id, ArticleRequest articleRequest) {
        log.info("Update article with id:{}, {}", id, articleRequest);

        Article article = articleMapper.requestToModel(articleRequest);
        List<Hashtag> hashtags = articleRequest.getHashtags()
                .stream()
                .map(hashtagService::checkIfExists)
                .collect(Collectors.toList());
        article.setHashtags(hashtags);
        article.setCreateDate(LocalDateTime.now());
        article.setId(id);
        article.setRating(ratingService.getMeanRatingByArticleId(id));

        articleRepository.save(article);
        return articleMapper.modelToResponse(article);
    }

    @Override
    public double rateArticle(RatingRequest ratingRequest) {
        int id = ratingRequest.getArticle().getId();
        log.info("Update article rating with id:{}", id);

        ratingService.rateArticle(ratingRequest);

        double rating = ratingService.getMeanRatingByArticleId(id);
        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found article with id:" + id));
        article.setRating(rating);

        articleRepository.save(article);

        return rating;
    }

    @Override
    public void delete(int id) {
        log.info("Delete article with id:{}", id);
        articleRepository.deleteById(id);
    }

}
