package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.UserRequest;
import com.kutylo.petclub.dto.response.MultiElementResponse;
import com.kutylo.petclub.dto.response.UserResponse;
import com.kutylo.petclub.service.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/users")
public class UserController {

    private UserService userService;


    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MultiElementResponse<UserResponse> getAllUsers() {
        return new MultiElementResponse<UserResponse>(userService.getAllUsers());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = UserResponse.class)
    })
    UserResponse getUserById(@PathVariable int id) {
        return userService.getUserById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/cluster/{username}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = UserResponse.class)
    })
    String getUserCluster(@PathVariable String username) {
        return userService.getUserCluster(username);
    }


    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/username/{username}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = UserResponse.class)
    })
    Map<String, UserResponse> getUserByUsername(@PathVariable String username) {

        return Map.of("user", userService.getUserByUsername(username));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User updated", response = UserResponse.class)
    })
    Map<String, UserResponse> update(@PathVariable int id, @RequestBody @Validated UserRequest userRequest) {
        return Map.of("user", userService.update(id, userRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User deleted")
    })
    void delete(@PathVariable int id) {
        userService.delete(id);
    }
}
