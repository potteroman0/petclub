package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.QuestionRequest;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.dto.response.QuestionResponse;
import com.kutylo.petclub.mapper.QuestionMapper;
import com.kutylo.petclub.model.CategoryType;
import com.kutylo.petclub.model.Question;
import com.kutylo.petclub.repository.QuestionRepository;
import com.kutylo.petclub.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionServiceIml implements QuestionService {

    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper;

    @Override
    public QuestionResponse getQuestionById(int id) {
        log.info("Get question with id: {}", id);
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found question with id: " + id));
        return questionMapper.modelToResponse(question);
    }

    @Override
    public List<String> getAllCategories() {
        log.info("Get all categories");
        return Arrays.stream(CategoryType.values()).map(CategoryType::getName).collect(Collectors.toList());
    }

    @Override
    public PageResponse<QuestionResponse> getAllResolvedQuestions(PaginationRequest paginationRequest) {
        log.info("Get all resolved question : {}", paginationRequest);

        Pageable pageable = paginationRequest.toPageable();
        Page<Question> questions = questionRepository.findQuestionsByIsResolvedTrue(pageable);
        PageResponse<QuestionResponse> pageResponse = new PageResponse<QuestionResponse>(
                questions.getTotalPages(),
                questions.getTotalElements(),
                questions.map(questionMapper::modelToResponse)
                        .get().collect(Collectors.toList())
        );

        return pageResponse;
    }

    @Override
    public PageResponse<QuestionResponse> getAllQuestions(PaginationRequest paginationRequest) {
        log.info("Get all question : {}", paginationRequest);

        Pageable pageable = paginationRequest.toPageable();
        Page<Question> questions = questionRepository.findAll(pageable);
        PageResponse<QuestionResponse> pageResponse = new PageResponse<QuestionResponse>(
                questions.getTotalPages(),
                questions.getTotalElements(),
                questions.map(questionMapper::modelToResponse)
                        .get().collect(Collectors.toList())
        );

        return pageResponse;
    }

    @Override
    public PageResponse<QuestionResponse> getAllQuestionsByUserId(int id, PaginationRequest paginationRequest) {
        log.info("Get all question by user id: {}, {}", paginationRequest, id);

        Pageable pageable = paginationRequest.toPageable();
        Page<Question> questions = questionRepository.findQuestionsByUserId(id, pageable);
        PageResponse<QuestionResponse> pageResponse = new PageResponse<QuestionResponse>(
                questions.getTotalPages(),
                questions.getTotalElements(),
                questions.map(questionMapper::modelToResponse)
                        .get().collect(Collectors.toList())
        );

        return pageResponse;
    }

    @Override
    public PageResponse<QuestionResponse> getAllQuestionsByCategoryName(String category, PaginationRequest paginationRequest) {
        log.info("Get all question by category: {}, {}", paginationRequest, category);

        Pageable pageable = paginationRequest.toPageable();
        Page<Question> questions = questionRepository.findQuestionsByCategory(CategoryType.valueOf(category.toUpperCase()), pageable);

        PageResponse<QuestionResponse> pageResponse = new PageResponse<QuestionResponse>(
                questions.getTotalPages(),
                questions.getTotalElements(),
                questions.map(questionMapper::modelToResponse)
                        .get().collect(Collectors.toList())
        );

        return pageResponse;
    }

    @Override
    public QuestionResponse save(QuestionRequest questionRequest) {
        log.info("Create question: {}", questionRequest);

        Question question = questionMapper.requestToModel(questionRequest);

        question.setCreateDate(LocalDateTime.now());
        questionRepository.save(question);
        return questionMapper.modelToResponse(question);
    }

    @Override
    public QuestionResponse update(int id, QuestionRequest questionRequest) {
        log.info("Update question with id: {},: {}", id, questionRequest);

        Question question = questionMapper.requestToModel(questionRequest);
        question.setId(id);
        question.setCreateDate(LocalDateTime.now());
        questionRepository.save(question);

        return questionMapper.modelToResponse(question);
    }

    @Override
    public QuestionResponse resolve(int id) {
        log.info("Resolve question with id: {}", id);

        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found question with id: " + id));
        question.setResolved(!question.isResolved());
        questionRepository.save(question);
        return questionMapper.modelToResponse(question);
    }

    @Override
    public void delete(int id) {
        log.info("Delete question with id: {}", id);
        questionRepository.deleteById(id);
    }
}
