package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.RatingRequest;
import com.kutylo.petclub.mapper.ArticleMapper;
import com.kutylo.petclub.mapper.RatingMapper;
import com.kutylo.petclub.mapper.UserMapper;
import com.kutylo.petclub.model.Rating;
import com.kutylo.petclub.repository.RatingRepository;
import com.kutylo.petclub.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;
    private final UserMapper userMapper;
    private final ArticleMapper articleMapper;
    private final RatingMapper ratingMapper;

    @Override
    public double rateArticle(RatingRequest ratingRequest) {
        log.info("Rate article:{}", ratingRequest);

        Rating rating = ratingMapper.requestToModel(ratingRequest);
        ratingRepository.save(rating);
        return getMeanRatingByArticleId(ratingRequest.getArticle().getId());
    }

    @Override
    public double getMeanRatingByArticleId(int id) {
        log.info("Get mean rating for article with id:{}", id);

        List<Rating> ratings = ratingRepository.findRatingsByArticleId(id)
                .orElse(Collections.emptyList());

        return ratings.stream()
                .filter(Objects::nonNull)
                .mapToDouble(Rating::getRate)
                .average()
                .orElse(0.0);
    }
}