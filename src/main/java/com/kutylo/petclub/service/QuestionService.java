package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.QuestionRequest;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.dto.response.QuestionResponse;

import java.util.List;

public interface QuestionService {
    QuestionResponse getQuestionById(int id);

    PageResponse<QuestionResponse> getAllQuestions(PaginationRequest paginationRequest);

    List<String> getAllCategories();

    PageResponse<QuestionResponse> getAllQuestionsByUserId(int id, PaginationRequest paginationRequest);

    PageResponse<QuestionResponse> getAllResolvedQuestions(PaginationRequest paginationRequest);

    PageResponse<QuestionResponse> getAllQuestionsByCategoryName(String category, PaginationRequest paginationRequest);

    QuestionResponse save(QuestionRequest questionRequest);

    QuestionResponse update(int id, QuestionRequest questionRequest);

    QuestionResponse resolve(int id);

    void delete(int id);
}
