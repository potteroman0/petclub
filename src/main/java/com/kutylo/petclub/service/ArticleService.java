package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.ArticleRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.RatingRequest;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.PageResponse;

public interface ArticleService {

    ArticleResponse getArticleById(int id);

    PageResponse<ArticleResponse> getAllArticles(PaginationRequest paginationRequest);

    PageResponse<ArticleResponse> getAllArticlesByHashtag(String hashtag, PaginationRequest paginationRequest);

    PageResponse<ArticleResponse> getAllArticleByUserId(int id, PaginationRequest paginationRequest);

    ArticleResponse save(ArticleRequest articleRequest);

    ArticleResponse update(int id, ArticleRequest articleRequest);

    double rateArticle(RatingRequest ratingRequest);

    void delete(int id);
}
