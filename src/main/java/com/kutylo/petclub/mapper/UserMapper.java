package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.UserRequest;
import com.kutylo.petclub.dto.response.UserResponse;
import com.kutylo.petclub.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User requestToModel(UserRequest userRequest);

    UserResponse modelToResponse(User user);

}
