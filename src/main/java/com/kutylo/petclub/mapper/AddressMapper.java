package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.AddressRequest;
import com.kutylo.petclub.dto.response.AddressResponse;
import com.kutylo.petclub.model.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    Address requestToModel(AddressRequest addressRequest);
    AddressResponse modelToResponse(Address address);
}
