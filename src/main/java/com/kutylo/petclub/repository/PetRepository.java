package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PetRepository extends JpaRepository<Pet, Integer> {
    Optional<List<Pet>> findPetsByUserId(int id);
}
