package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.QuestionAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionAnswerRepository extends JpaRepository<QuestionAnswer, Integer> {
    Optional<List<QuestionAnswer>> findQuestionAnswersByQuestionId(int id);
}
