package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.QuestionAnswerRequest;
import com.kutylo.petclub.dto.response.QuestionAnswerResponse;
import com.kutylo.petclub.model.QuestionAnswer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface QuestionAnswerMapper {
    QuestionAnswer requestToModel(QuestionAnswerRequest questionAnswerRequest);

    QuestionAnswerResponse modelToResponse(QuestionAnswer questionAnswer);
}
