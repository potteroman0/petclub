package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.RatingRequest;
import com.kutylo.petclub.model.Rating;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RatingMapper {
    Rating requestToModel(RatingRequest ratingRequest);
}
