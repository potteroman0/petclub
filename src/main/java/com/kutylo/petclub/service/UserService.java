package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.UserRequest;
import com.kutylo.petclub.dto.response.UserResponse;

import java.util.List;

public interface UserService {
    List<UserResponse> getAllUsers();

    UserResponse getUserById(int id);

    String getUserCluster(String username);

    UserResponse getCurrentUser();

    UserResponse getUserByUsername(String username);

    UserResponse save(UserRequest userRequest);

    UserResponse update(int id, UserRequest userRequest);

    void delete(int id);
}
