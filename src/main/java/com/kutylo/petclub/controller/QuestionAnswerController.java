package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.QuestionAnswerRequest;
import com.kutylo.petclub.dto.response.CommentResponse;
import com.kutylo.petclub.dto.response.QuestionAnswerResponse;
import com.kutylo.petclub.model.QuestionAnswer;
import com.kutylo.petclub.service.QuestionAnswerService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/answers")
public class QuestionAnswerController {

    private final QuestionAnswerService questionAnswerService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/question/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comments found")
    })
    public Map<String, List<QuestionAnswerResponse>> getAnswersByQuestionId(@PathVariable int id) {
        return Map.of("answers", questionAnswerService.getAnswersByQuestionId(id));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Comment created", response = CommentResponse.class)
    })
    public Map<String, QuestionAnswerResponse> save(@RequestBody QuestionAnswerRequest questionAnswerRequest) {
        return Map.of("answer", questionAnswerService.save(questionAnswerRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/mark/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Answer marked", response = QuestionAnswerResponse.class)
    })
    public Map<String, QuestionAnswerResponse> markAsRight(@PathVariable int id) {
        return Map.of("answer", questionAnswerService.markAsRight(id));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        questionAnswerService.delete(id);
    }
}
