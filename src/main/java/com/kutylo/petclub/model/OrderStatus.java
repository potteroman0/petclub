package com.kutylo.petclub.model;

public enum OrderStatus {
    DONE("done"), IN_PROGRESS("in progress");

    private String status;

    OrderStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
