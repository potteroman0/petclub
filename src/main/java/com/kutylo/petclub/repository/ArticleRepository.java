package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {
    Page<Article> findArticlesByUserId(int id, Pageable pageable);

    Page<Article> findArticlesByHashtagsName(String name, Pageable pageable);

    Page<Article> findArticleByHashtagsId(int id, Pageable pageable);
}
