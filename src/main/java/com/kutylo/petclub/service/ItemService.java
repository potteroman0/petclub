package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.ItemRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.ItemResponse;
import com.kutylo.petclub.dto.response.PageResponse;

import java.util.List;

public interface ItemService {
    ItemResponse getItemById(int id);

    PageResponse<ItemResponse> getAllItems(PaginationRequest paginationRequest);

    List<String> getAllItemCategory();

    List<String> getAllItemPetType();

    PageResponse<ItemResponse> getAllItemsByCategory(String category, PaginationRequest paginationRequest);

    PageResponse<ItemResponse> getAllItemsByPetType(String petType, PaginationRequest paginationRequest);

    ItemResponse save(ItemRequest itemRequest);

    ItemResponse update(int id, ItemRequest itemRequest);

    void delete(int id);
}
