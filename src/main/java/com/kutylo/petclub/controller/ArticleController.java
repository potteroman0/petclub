package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.ArticleRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.RatingRequest;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.service.ArticleService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/articles")
public class ArticleController {

    private final ArticleService articleService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/rate",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Article rated", response = Double.class)
    })
    Map<String, Double> rateArticle(@RequestBody RatingRequest ratingRequest) {
       return Map.of("rating" , articleService.rateArticle(ratingRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found", response = ArticleResponse.class)
    })
    Map<String, PageResponse<ArticleResponse>> getAllArticles(@Validated PaginationRequest paginationRequest) {
        return Map.of("articles", articleService.getAllArticles(paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found", response = ArticleResponse.class)
    })
    Map<String, ArticleResponse> getArticleById(@PathVariable int id) {
        return Map.of("article", articleService.getArticleById(id));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/hashtag/{hashtag}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles found", response = ArticleResponse.class)
    })
    Map<String, PageResponse<ArticleResponse>> getAllArticlesByHashtag(@PathVariable String hashtag, @Validated PaginationRequest paginationRequest) {
        return Map.of("articles", articleService.getAllArticlesByHashtag(hashtag, paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Article found")
    })
    Map<String, PageResponse<ArticleResponse>> getAllArticleByUserId(@PathVariable int id, @Validated PaginationRequest paginationRequest) {
        return Map.of("articles", articleService.getAllArticleByUserId(id, paginationRequest));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Article created", response = ArticleResponse.class)
    })
    Map<String, ArticleResponse> save(@RequestBody ArticleRequest articleRequest) {
        return Map.of("article", articleService.save(articleRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Articles updated")
    })
    Map<String, ArticleResponse> update(@PathVariable int id, @RequestBody @Validated ArticleRequest articleRequest) {
        return Map.of("article", articleService.update(id, articleRequest));
    }


    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Article deleted")
    })
    void delete(@PathVariable int id) {
        articleService.delete(id);
    }
}
