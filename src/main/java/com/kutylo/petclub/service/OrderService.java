package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.OrderResponse;
import com.kutylo.petclub.dto.response.PageResponse;

public interface OrderService {
    PageResponse<OrderResponse> getOrdersByUserId(int id, PaginationRequest paginationRequest);
}
