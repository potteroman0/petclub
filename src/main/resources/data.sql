INSERT INTO User_account(username, password, email, phone, image_url)
VALUES ('Dima', '$2y$10$MGkvjuXgB4cHhoxznv2d8uTpPp5dzif9aYAghyxWDv5VxloCK.Emu', 'dima@gmail.com', '+380556541223',
        'https://res.cloudinary.com/petclub/image/upload/v1615141240/user/default.png.png'),
       ('Andriy', 'pass123', 'andriy@gmail.com', '+380556241123',
        'https://res.cloudinary.com/petclub/image/upload/v1615141240/user/default.png.png');

INSERT INTO Article(title, text, extended_text, image_url, create_date, likes_amount, user_id)
VALUES ('how to wash pet', 'When you want...', 'When you want...', 'https://res.cloudinary.com/petclub/image/upload/v1615144269/articles/image2.png.jpg', '2020-12-13', 1, 1),
       ('how to walk with pet', 'When you want...', 'When you want...', 'https://res.cloudinary.com/petclub/image/upload/v1615144269/articles/image2.png.jpg', '2020-05-13', 2, 2),
       ('how to love your pet', 'When you want...', 'When you want...', 'https://res.cloudinary.com/petclub/image/upload/v1615144269/articles/image2.png.jpg', '2021-07-10', 1, 1),
       ('how to play with your pet', 'When you want...', 'When you want...', 'https://res.cloudinary.com/petclub/image/upload/v1615144269/articles/image2.png.jpg', '2020-11-13', 0, 2),
       ('how to wash pet and to dry him', 'When you want...', 'When you want...', 'https://res.cloudinary.com/petclub/image/upload/v1615144269/articles/image2.png.jpg', '2021-02-13', 3, 1);

INSERT INTO Pet(pet_type, name, age, image_url, user_id)
VALUES ('DOG', 'muha', 15, 'https://res.cloudinary.com/petclub/image/upload/v1615281916/user-pets/default_pet_zquoff.jpg', 1),
       ('CAT', 'dymka', 2, 'https://res.cloudinary.com/petclub/image/upload/v1615281916/user-pets/default_pet_zquoff.jpg', 2);

INSERT INTO Comment(text, create_date, user_id, article_id)
VALUES ('very good article', '2021-02-11', 2, 2),
       ('not bad', '2021-03-21', 1, 1),
       ('great', '2021-04-02', 2, 3),
       ('nice', '2021-04-02', 1, 4),
       ('interesting article', '2021-04-02', 2, 5);

INSERT INTO Hashtag(name)
VALUES ('eat'),
       ('walk'),
       ('health');

INSERT INTO Article_hashtag(article_id, hashtag_id)
VALUES (1, 1),
       (1, 2),
       (2, 1),
       (3, 3),
       (4, 2),
       (4, 2),
       (5, 3);