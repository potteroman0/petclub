package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
    Page<Item> findItemsByType(ItemCategory category, Pageable pageable);

    Page<Item> findItemsByPetType(PetType petType, Pageable pageable);
}
