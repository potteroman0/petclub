package com.kutylo.petclub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@Entity
//@Table(name = "Address")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Id
    private int id;
    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @Column(name = "build")
    private String build;

    @OneToOne(mappedBy = "address")
    private User user;
}
