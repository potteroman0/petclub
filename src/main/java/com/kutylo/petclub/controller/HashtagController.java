package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.HashtagRequest;
import com.kutylo.petclub.dto.response.HashtagResponse;
import com.kutylo.petclub.service.HashtagService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/hashtags")
public class HashtagController {

    private final HashtagService hashtagService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hashtags found", response = HashtagResponse.class)
    })
    Map<String, List<HashtagResponse>> getAllHashtags() {
        return Map.of("hashtags", hashtagService.getAllHashtags());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Hashtag created", response = HashtagResponse.class)
    })
    Map<String, HashtagResponse> save(@RequestBody @Validated HashtagRequest hashtagRequest) {
        return Map.of("hashtag", hashtagService.save(hashtagRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Hashtags deleted")
    })
    void delete(@PathVariable int id) {
        hashtagService.delete(id);
    }
}
