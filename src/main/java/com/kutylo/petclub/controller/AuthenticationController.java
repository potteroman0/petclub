package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.AuthenticationRequest;
import com.kutylo.petclub.dto.request.UserRequest;
import com.kutylo.petclub.dto.response.AuthenticationResponse;
import com.kutylo.petclub.dto.response.SingleElementResponse;
import com.kutylo.petclub.dto.response.UserResponse;
import com.kutylo.petclub.service.AuthenticationService;
import com.kutylo.petclub.service.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationService authenticationService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = UserResponse.class)
    })
    Map<String,UserResponse> getCurrentUser() {

        return Map.of("user", userService.getCurrentUser());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User login successfully", response = UserResponse.class)
    })
    public Map<String, AuthenticationResponse> login(@Validated @RequestBody AuthenticationRequest authenticationRequest) {
        return Map.of("user", authenticationService.login(authenticationRequest));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "User created", response = UserResponse.class)
    })
    SingleElementResponse<UserResponse> register(@RequestBody @Validated UserRequest userRequest) {
        return new SingleElementResponse<UserResponse>("user", userService.save(userRequest));
    }
}
