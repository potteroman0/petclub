package com.kutylo.petclub.model;

import java.util.Arrays;
import java.util.Optional;

public enum PetType {
    DOG("dog"), CAT("cat");

    private String name;

    PetType(String type) {
        this.name = type;
    }

    public static PetType fromName(String name) {
        Optional<PetType> petType =
                Arrays.stream(values())
                        .filter(t->t.getName().equalsIgnoreCase(name))
                        .findFirst();
        if (petType.isPresent()) {
            return petType.get();
        } else {
            throw new UnsupportedOperationException(
                    "The name " + name + " is not supported!"
            );
        }
    }

    public String getName() {
        return name;
    }
}
