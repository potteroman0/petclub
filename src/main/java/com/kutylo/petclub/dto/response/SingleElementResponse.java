package com.kutylo.petclub.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;

@Data
@AllArgsConstructor
public class SingleElementResponse<T> {
    private HashMap<String, T> data;

    public SingleElementResponse(String key, T value){
        this.data = new HashMap<String, T>();
        this.data.put(key, value);
    }
}
