package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArticleRequest {
    @NotNull
    private String title;
    private String text;
    private UserResponse user;
    @Builder.Default
    private List<HashtagRequest> hashtags = new ArrayList<>();
}
