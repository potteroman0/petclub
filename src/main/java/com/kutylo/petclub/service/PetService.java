package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.PetRequest;
import com.kutylo.petclub.dto.response.PetResponse;

import java.util.List;

public interface PetService {
    List<PetResponse> getPetsByUserId(int id);

    PetResponse getPetById(int id);

    PetResponse save(PetRequest petRequest);

    PetResponse update(int id, PetRequest petRequest);

    void delete(int id);
}
