package com.kutylo.petclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PetclubApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetclubApplication.class, args);
    }

}
