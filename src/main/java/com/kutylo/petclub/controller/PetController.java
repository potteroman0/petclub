package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.PetRequest;
import com.kutylo.petclub.dto.response.PetResponse;
import com.kutylo.petclub.service.PetService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/pets")
public class PetController {
    private PetService petService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pet found")
    })
    public List<PetResponse> getPetsByUserId(@PathVariable int id) {
        return petService.getPetsByUserId(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pet found")
    })
    PetResponse getPetById(@PathVariable int id) {
        return petService.getPetById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pet created", response = PetResponse.class)
    })
    PetResponse save(@RequestBody @Validated PetRequest petRequest) {
        return petService.save(petRequest);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pet updated", response = PetResponse.class)
    })
    PetResponse update(@PathVariable int id, @RequestBody @Validated PetRequest petRequest) {
        return petService.update(id, petRequest);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Pet delete")
    })
    void delete(@PathVariable int id) {
        petService.delete(id);
    }
}
