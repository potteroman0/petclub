package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.cloudinaryapi.CloudinaryManager;
import com.kutylo.petclub.service.PhotoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Slf4j
@RequiredArgsConstructor
public class PhotoServiceImpl implements PhotoService {

    private final CloudinaryManager cloudinaryManager;

    @Override
    public String uploadPhoto(String folder, MultipartFile image) throws IOException {
        log.info("Upload photo to folder:{}", folder);
        return cloudinaryManager.uploadImage(folder, image);
    }
}
