package com.kutylo.petclub.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PhotoService {
    String uploadPhoto(String folder, MultipartFile image) throws IOException;
}
