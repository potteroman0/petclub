package com.kutylo.petclub.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetResponse {
    private int id;
    private String petType;
    private String name;
    private Integer age;
    private String imgUrl;
}
