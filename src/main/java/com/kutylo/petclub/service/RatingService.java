package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.RatingRequest;

public interface RatingService {
    double getMeanRatingByArticleId(int id);

    double rateArticle(RatingRequest ratingRequest);
}
