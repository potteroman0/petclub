package com.kutylo.petclub.security.jwt;

import com.kutylo.petclub.model.Role;
import com.kutylo.petclub.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;

public final class JwtUserFactory {

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getRole())
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(Role role) {
        SimpleGrantedAuthority authority;
        switch (role) {
            case ADMIN:
                authority = new SimpleGrantedAuthority("ADMIN");
                break;
            case MODERATOR:
                authority = new SimpleGrantedAuthority("MODERATOR");
                break;
            default:
                authority = new SimpleGrantedAuthority(("USER"));
                break;
        }
        return Collections.singletonList(authority);
    }

}
