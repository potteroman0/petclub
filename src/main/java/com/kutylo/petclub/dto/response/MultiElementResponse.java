package com.kutylo.petclub.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MultiElementResponse<T> {
    List<T> data;
}
