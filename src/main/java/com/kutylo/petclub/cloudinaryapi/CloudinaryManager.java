package com.kutylo.petclub.cloudinaryapi;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CloudinaryManager {

    String uploadImage(String folder, MultipartFile image) throws IOException;
}
