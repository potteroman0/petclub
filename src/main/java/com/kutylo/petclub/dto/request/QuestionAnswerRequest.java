package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.QuestionResponse;
import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionAnswerRequest {
    private String text;
    private String imgUrl;
    private UserResponse user;
    private QuestionResponse question;
}
