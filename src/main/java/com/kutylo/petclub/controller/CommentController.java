package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.CommentRequest;
import com.kutylo.petclub.dto.response.CommentResponse;
import com.kutylo.petclub.service.CommentService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/comments")
public class CommentController {

    private final CommentService commentService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/article/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comments found", response = CommentResponse.class)
    })
    Map<String, List<CommentResponse>> getAllCommentsByArticleId(@PathVariable int id) {
        return Map.of("comments", commentService.getAllCommentsByArticleId(id));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comments found", response = CommentResponse.class)
    })
    Map<String, List<CommentResponse>> getAllCommentsByUserId(@PathVariable int id) {
        return Map.of("comments", commentService.getAllCommentsByUserId(id));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Comment created", response = CommentResponse.class)
    })
    Map<String, CommentResponse> save(@RequestBody @Validated CommentRequest commentRequest) {
        return Map.of("comment", commentService.save(commentRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    void delete(@PathVariable int id) {
        commentService.delete(id);
    }
}
