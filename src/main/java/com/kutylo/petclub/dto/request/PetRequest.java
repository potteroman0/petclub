package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetRequest {
    @NotNull
    private String petType;
    @NotNull
    private String name;
    @Min(value = 1, message = "Invalid pet age")
    @Max(value = 20, message = "Invalid pet age")
    private Integer age;
    private String imgUrl;
    private UserResponse user;
}
