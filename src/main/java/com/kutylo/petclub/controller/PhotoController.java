package com.kutylo.petclub.controller;

import com.kutylo.petclub.service.PhotoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
public class PhotoController {

    private final PhotoService photoService;

    @RequestMapping(value = "/photos",
            method = RequestMethod.POST)
    @ApiOperation(value = "Upload photo")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Photo successful upload", response = String.class),
            @ApiResponse(code = 400, message = "Can`t upload photo")
    })
    public Map<String, String> uploadPhoto(@RequestParam("folder") String folder, @RequestParam("file") MultipartFile image) throws IOException {
        return Map.of("url", photoService.uploadPhoto(folder, image));
    }
}

