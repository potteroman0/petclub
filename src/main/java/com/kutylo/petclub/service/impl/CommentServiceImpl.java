package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.CommentRequest;
import com.kutylo.petclub.dto.response.CommentResponse;
import com.kutylo.petclub.mapper.CommentMapper;
import com.kutylo.petclub.model.Comment;
import com.kutylo.petclub.repository.CommentRepository;
import com.kutylo.petclub.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;

    @Override
    public List<CommentResponse> getAllCommentsByArticleId(int id) {
        log.info("Get comments by article id:{}", id);

        return commentRepository.findCommentsByArticleId(id)
                .orElse(Collections.emptyList())
                .stream()
                .map(commentMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<CommentResponse> getAllCommentsByUserId(int id) {
        log.info("Get comments by user id:{}", id);

        return commentRepository.findCommentsByUserId(id)
                .orElse(Collections.emptyList())
                .stream()
                .map(commentMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public CommentResponse save(CommentRequest commentRequest) {
        log.info("Save comment:{}", commentRequest);

        Comment comment = commentMapper.requestToModel(commentRequest);
        comment.setCreatedDate(LocalDateTime.now());

        commentRepository.save(comment);
        return commentMapper.modelToResponse(comment);
    }

    @Override
    public void delete(int id) {
        log.info("Deleting comment with id: {}", id);
        commentRepository.deleteById(id);
    }
}
