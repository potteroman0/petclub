package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.AdvertisementRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.AdvertisementResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.service.AdvertisementService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping(value = "/advertisements")
public class AdvertisementController {

    private final AdvertisementService advertisementService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Advertisements found")
    })
    Map<String, PageResponse<AdvertisementResponse>> getAllAdvertisements(@Validated PaginationRequest paginationRequest) {
        return Map.of("advertisements", advertisementService.getAllAdvertisements(paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Advertisement found")
    })
    Map<String, AdvertisementResponse> getAdvertisementById(@PathVariable int id) {
        return Map.of("advertisement", advertisementService.getAdvertisementById(id));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Advertisement created", response = AdvertisementResponse.class)
    })
    Map<String, AdvertisementResponse> save(@RequestBody AdvertisementRequest advertisement) {
        return Map.of("advertisement", advertisementService.save(advertisement));

    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Advertisement updated")
    })
    Map<String, AdvertisementResponse> update(@PathVariable int id, @RequestBody AdvertisementRequest advertisement) {
        return Map.of("advertisement", advertisementService.update(id, advertisement));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Advertisement deleted")
    })
    void delete(@PathVariable int id) {
        advertisementService.delete(id);
    }
}
