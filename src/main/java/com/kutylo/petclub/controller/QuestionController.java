package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.request.QuestionRequest;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.dto.response.QuestionResponse;
import com.kutylo.petclub.service.QuestionService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/questions")
public class QuestionController {

    private final QuestionService questionService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/categories",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Categories found")
    })
    Map<String, List<String>> getAllCategories() {
        return Map.of("categories", questionService.getAllCategories());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Questions found")
    })
    Map<String, PageResponse<QuestionResponse>> getAllQuestions(@Validated PaginationRequest paginationRequest) {
        return Map.of("questions", questionService.getAllQuestions(paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/category/{category}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Questions found")
    })
    Map<String, PageResponse<QuestionResponse>> getQuestionsByCategory(@PathVariable String category, @Validated PaginationRequest paginationRequest) {
        return Map.of("questions", questionService.getAllQuestionsByCategoryName(category, paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "user/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Question found")
    })
    Map<String, PageResponse<QuestionResponse>> getQuestionByUserId(@PathVariable int id, @Validated PaginationRequest paginationRequest) {
        return Map.of("questions", questionService.getAllQuestionsByUserId(id, paginationRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Question found")
    })
    Map<String, QuestionResponse> getQuestionById(@PathVariable int id) {
        return Map.of("question", questionService.getQuestionById(id));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/resolved",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Questions found")
    })
    Map<String, PageResponse<QuestionResponse>> getAllResolvedQuestions(PaginationRequest paginationRequest) {
        return Map.of("questions", questionService.getAllResolvedQuestions(paginationRequest));
    }
    //------------------------------

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Question created")
    })
    Map<String, QuestionResponse> save(@RequestBody QuestionRequest questionRequest) {
        return Map.of("question", questionService.save(questionRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Question updated")
    })
    Map<String, QuestionResponse> update(@PathVariable int id, @RequestBody QuestionRequest questionRequest) {
        return Map.of("question", questionService.update(id, questionRequest));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/resolve/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Question mark as resolved")
    })
    Map<String, QuestionResponse> resolve(@PathVariable int id) {
        return Map.of("question", questionService.resolve(id));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Article deleted")
    })
    void delete(@PathVariable int id) {
        questionService.delete(id);
    }

}
