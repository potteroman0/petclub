-- CREATE TABLE IF NOT EXISTS User_account
-- (
--     id        INT serial PRIMARY KEY,
--     username  VARCHAR(32)  NOT NULL UNIQUE,
--     password  VARCHAR(255) NOT NULL,
--     email     VARCHAR(32)  NOT NULL UNIQUE,
--     phone     VARCHAR(13)  NOT NULL UNIQUE,
--     image_url VARCHAR(255)
-- );
-- -- //////////////////////////////////////////////////
-- CREATE TABLE IF NOT EXISTS Article
-- (
--     id            INT serial PRIMARY KEY,
--     title         VARCHAR(225) NOT NULL,
--     text          VARCHAR(225) NOT NULL,
--     extended_text VARCHAR(255) NOT NULl,
--     image_url     VARCHAR(255),
--     create_date   VARCHAR(52),
--     likes_amount  INT,
--     user_id       INT          NOT NULL
-- );
-- ALTER TABLE Article
--     ADD FOREIGN KEY (user_id)
--         REFERENCES User_account (id);
--
-- --///////////////////////////////////////////////////
-- CREATE TABLE IF NOT EXISTS Pet
-- (
--     id        INT serial PRIMARY KEY,
--     pet_type  VARCHAR(32) NOT NULL,
--     name      VARCHAR(32) NOT NULL,
--     age       INT         NOT NULl,
--     image_url VARCHAR(255),
--     user_id   INT         NOT NULL
-- );
-- ALTER TABLE Pet
--     ADD FOREIGN KEY (user_id)
--         REFERENCES User_account (id);
--
-- --////////////////////////////////////////////////////
-- CREATE TABLE Comment
-- (
--     id          INT serial PRIMARY KEY,
--     text        VARCHAR(255) NOT NULL,
--     create_date VARCHAR(52)  NOT NULL,
--     user_id     INT          NOT NULL,
--     article_id  INT          NOT NULL
-- );
--
-- ALTER TABLE Comment
--     ADD FOREIGN KEY (user_id)
--         REFERENCES User_account (id);
-- ALTER TABLE Comment
--     ADD FOREIGN KEY (article_id)
--         REFERENCES Article (id);
--
-- --//////////////////////////////////////////////////////
-- CREATE TABLE Hashtag
-- (
--     id   INT serial PRIMARY KEY,
--     name VARCHAR(32) NOT NULL UNIQUE
-- );
-- --/////////////////////////////////////////////////////
-- CREATE TABLE Article_hashtag
-- (
--     id         INT serial NOT NULL,
--     article_id INT        NOT NULL,
--     hashtag_id INT        NOT NULL
-- );
-- ALTER TABLE Article_hashtag
--     ADD FOREIGN KEY (hashtag_id)
--         REFERENCES Hashtag (id);
-- ALTER TABLE Article_hashtag
--     ADD FOREIGN KEY (article_id)
--         REFERENCES Article (id);
--
-- --//////////////
--
--
--
