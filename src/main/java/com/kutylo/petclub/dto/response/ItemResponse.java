package com.kutylo.petclub.dto.response;

import com.kutylo.petclub.model.CategoryType;
import com.kutylo.petclub.model.ItemCategory;
import com.kutylo.petclub.model.PetType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemResponse {
    private int id;
    private String title;
    private String description;
    private int amount;
    private double price;
    private ItemCategory type;
    private PetType petType;
    private String imgUrl;
}

