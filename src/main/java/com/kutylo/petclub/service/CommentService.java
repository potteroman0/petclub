package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.CommentRequest;
import com.kutylo.petclub.dto.response.CommentResponse;

import java.util.List;

public interface CommentService {
    List<CommentResponse> getAllCommentsByArticleId(int id);

    List<CommentResponse> getAllCommentsByUserId(int id);

    CommentResponse save(CommentRequest commentRequest);

    void delete(int id);
}
