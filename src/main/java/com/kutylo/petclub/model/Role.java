package com.kutylo.petclub.model;

public enum Role {
    USER("user"), ADMIN("admin"), MODERATOR("moderator");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
