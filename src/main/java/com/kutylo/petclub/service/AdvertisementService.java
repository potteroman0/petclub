package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.AdvertisementRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.AdvertisementResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.model.Advertisement;

public interface AdvertisementService {

    PageResponse<AdvertisementResponse> getAllAdvertisements(PaginationRequest paginationRequest);

    AdvertisementResponse getAdvertisementById(int id);

    AdvertisementResponse save(AdvertisementRequest advertisement);

    AdvertisementResponse update(int id, AdvertisementRequest advertisement);

    void delete(int id);
}
