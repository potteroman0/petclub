package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.QuestionRequest;
import com.kutylo.petclub.dto.response.QuestionResponse;
import com.kutylo.petclub.model.Question;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface QuestionMapper {
    Question requestToModel(QuestionRequest questionRequest);

    QuestionResponse modelToResponse(Question question);
}
