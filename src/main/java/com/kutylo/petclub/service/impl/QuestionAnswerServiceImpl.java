package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.QuestionAnswerRequest;
import com.kutylo.petclub.dto.response.QuestionAnswerResponse;
import com.kutylo.petclub.mapper.QuestionAnswerMapper;
import com.kutylo.petclub.model.QuestionAnswer;
import com.kutylo.petclub.repository.QuestionAnswerRepository;
import com.kutylo.petclub.service.QuestionAnswerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class QuestionAnswerServiceImpl implements QuestionAnswerService {
    private final QuestionAnswerMapper questionAnswerMapper;
    private final QuestionAnswerRepository questionAnswerRepository;

    @Override
    public List<QuestionAnswerResponse> getAnswersByQuestionId(int id) {
        log.info("Get answers by question id:{}", id);
        return questionAnswerRepository.findQuestionAnswersByQuestionId(id)
                .orElse(Collections.emptyList())
                .stream().map(questionAnswerMapper::modelToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public QuestionAnswerResponse save(QuestionAnswerRequest questionAnswerRequest) {
        log.info("Save answer:{}", questionAnswerRequest);
        QuestionAnswer questionAnswer = questionAnswerMapper.requestToModel(questionAnswerRequest);
        questionAnswer.setCreateDate(LocalDateTime.now());
        questionAnswerRepository.save(questionAnswer);
        return questionAnswerMapper.modelToResponse(questionAnswer);
    }

    @Override
    public QuestionAnswerResponse update(int id, QuestionAnswerRequest questionAnswerRequest) {
        return null;
    }

    @Override
    public QuestionAnswerResponse markAsRight(int id) {
        log.info("Mark as right:{}", id);

        QuestionAnswer questionAnswer = questionAnswerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found entity with id:" + id));
        questionAnswer.setRightAnswer(!questionAnswer.isRightAnswer());
        questionAnswerRepository.save(questionAnswer);

        return questionAnswerMapper.modelToResponse(questionAnswer);

    }

    @Override
    public void delete(int id) {
        log.info("Delete answer with id:{}", id);
        questionAnswerRepository.deleteById(id);
    }
}
