package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.QuestionAnswerRequest;
import com.kutylo.petclub.dto.response.QuestionAnswerResponse;

import java.util.List;

public interface QuestionAnswerService {

    List<QuestionAnswerResponse> getAnswersByQuestionId(int id);

    QuestionAnswerResponse save(QuestionAnswerRequest questionAnswerRequest);

    QuestionAnswerResponse update(int id, QuestionAnswerRequest questionAnswerRequest);

    QuestionAnswerResponse markAsRight(int id);

    void delete(int id);
}
