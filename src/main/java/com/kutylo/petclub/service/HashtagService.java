package com.kutylo.petclub.service;

import com.kutylo.petclub.dto.request.HashtagRequest;
import com.kutylo.petclub.dto.response.HashtagResponse;
import com.kutylo.petclub.model.Hashtag;

import java.util.List;

public interface HashtagService {
    List<HashtagResponse> getAllHashtags();

    HashtagResponse getById(int id);

    Hashtag checkIfExists(HashtagRequest hashtag);

    HashtagResponse save(HashtagRequest hashtagRequest);

    void delete(int id);
}
