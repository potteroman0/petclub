package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.AuthenticationRequest;
import com.kutylo.petclub.dto.response.AuthenticationResponse;
import com.kutylo.petclub.mapper.UserMapper;
import com.kutylo.petclub.model.User;
import com.kutylo.petclub.repository.UserRepository;
import com.kutylo.petclub.security.jwt.JwtTokenProvider;
import com.kutylo.petclub.service.AuthenticationService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

@Service
@Data
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private BCryptPasswordEncoder encoder;

    @PostConstruct
    private void initEncoder() {
        encoder = new BCryptPasswordEncoder();
    }

    @Override
    public AuthenticationResponse login(AuthenticationRequest authenticationRequest) {
        log.info("Logging user with name = " + authenticationRequest.getUsername());
        User user = userRepository.findByUsername(authenticationRequest.getUsername())
                .orElseThrow(() -> new EntityNotFoundException("Not found user with username:" + authenticationRequest.getUsername()));
        if (!encoder.matches(authenticationRequest.getPassword(), user.getPassword())) {
            log.error("Password not correct");
            throw new IllegalArgumentException("Password not correct");
        }

        String token = jwtTokenProvider.createToken(user.getUsername(), false);
        return new AuthenticationResponse(
                user.getUsername(),
                token,
                userMapper.modelToResponse(user)
        );
    }
}
