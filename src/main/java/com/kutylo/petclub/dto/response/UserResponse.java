package com.kutylo.petclub.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private int id;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String imgUrl;
    private String role;
}
