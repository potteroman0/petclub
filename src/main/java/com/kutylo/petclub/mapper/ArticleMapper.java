package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.ArticleRequest;
import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.model.Article;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ArticleMapper {
    ArticleResponse modelToResponse(Article article);

    Article requestToModel(ArticleRequest articleRequest);
}
