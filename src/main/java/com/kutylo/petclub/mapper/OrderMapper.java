package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.response.OrderResponse;
import com.kutylo.petclub.model.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderResponse modelToResponse(Order order);
}
