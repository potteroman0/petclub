package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.PetRequest;
import com.kutylo.petclub.dto.response.PetResponse;
import com.kutylo.petclub.model.Pet;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PetMapper {
    Pet requestToModel(PetRequest petRequest);

    PetResponse modelToResponse(Pet pet);

}
