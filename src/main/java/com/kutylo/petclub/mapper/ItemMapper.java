package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.ItemRequest;
import com.kutylo.petclub.dto.response.ItemResponse;
import com.kutylo.petclub.model.Item;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ItemMapper {

    Item requestToModel(ItemRequest itemRequest);

    ItemResponse modelToResponse(Item item);
}
