package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.OrderResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.mapper.OrderMapper;
import com.kutylo.petclub.model.Order;
import com.kutylo.petclub.repository.OrderRepository;
import com.kutylo.petclub.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;

    @Override
    public PageResponse<OrderResponse> getOrdersByUserId(int id, PaginationRequest paginationRequest) {
        log.info("Get orders by user id:{}", id);
        Pageable pageable = paginationRequest.toPageable();
        Page<Order> orderPage = orderRepository.findOrdersByUserId(id, pageable);

        PageResponse<OrderResponse> pageResponse = new PageResponse<>(
                orderPage.getTotalPages(),
                orderPage.getTotalElements(),
                orderPage.map(orderMapper::modelToResponse)
                        .get()
                        .collect(Collectors.toList())

        );

        return pageResponse;
    }
}
