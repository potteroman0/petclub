package com.kutylo.petclub.security;

import com.kutylo.petclub.model.User;
import com.kutylo.petclub.repository.UserRepository;
import com.kutylo.petclub.security.jwt.JwtUserFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;


@Service
@Data
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user by name = " + username);
        User user = userRepository.findByUsername(username)
                .orElseThrow(()->new EntityNotFoundException());
        return JwtUserFactory.create(user);
    }
}
