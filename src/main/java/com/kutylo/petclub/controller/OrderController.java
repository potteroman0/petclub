package com.kutylo.petclub.controller;

import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.OrderResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping(value = "/orders")
public class OrderController {

    private final OrderService orderService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    Map<String, PageResponse<OrderResponse>> getOrdersByUserId(@PathVariable int id, @Validated PaginationRequest paginationRequest) {
        return Map.of("orders", orderService.getOrdersByUserId(id, paginationRequest));
    }
}
