package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    Page<Order> findOrdersByUserId(int id, Pageable pageable);
}
