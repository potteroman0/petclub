package com.kutylo.petclub.repository;

import com.kutylo.petclub.model.CategoryType;
import com.kutylo.petclub.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> {
    Page<Question> findQuestionsByCategory(CategoryType category, Pageable pageable);

    Page<Question> findQuestionsByUserId(int id, Pageable pageable);

    Page<Question> findQuestionsByIsResolvedTrue(Pageable pageable);
}
