package com.kutylo.petclub.mapper;

import com.kutylo.petclub.dto.request.HashtagRequest;
import com.kutylo.petclub.dto.response.HashtagResponse;
import com.kutylo.petclub.model.Hashtag;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HashtagMapper {
    Hashtag requestToModel(HashtagRequest hashtagRequest);

    HashtagResponse modelToResponse(Hashtag hashtag);
}
