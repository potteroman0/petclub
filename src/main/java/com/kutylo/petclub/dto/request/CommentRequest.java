package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.ArticleResponse;
import com.kutylo.petclub.dto.response.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentRequest {
    @NotNull
    private String text;
    private UserResponse user;
    private ArticleResponse article;
}
