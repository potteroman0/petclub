package com.kutylo.petclub.cloudinaryapi.impl;

import com.cloudinary.Cloudinary;
import com.google.common.collect.ImmutableMap;
import com.kutylo.petclub.cloudinaryapi.CloudinaryManager;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class CloudinaryManagerImpl implements CloudinaryManager {

    private final Cloudinary cloudinary;

    private static final String PUBLIC_ID = "public_id";
    private static final String OVERWRITE = "overwrite";
    private static final String RESOURCE_TYPE = "resource-type";

    private static final String RES_TYPE = "image";
    private static final Boolean IS_OVERWRITE = true;

    @Override
    @SuppressWarnings("unchecked")
    public String uploadImage(String folder, MultipartFile image) throws IOException {

        Map<String, Object> params = ImmutableMap.of(
                PUBLIC_ID, folder + image.getOriginalFilename(),
                OVERWRITE, IS_OVERWRITE,
                RESOURCE_TYPE, RES_TYPE
        );

        Map<Object, Object> uploadResult = cloudinary.uploader().upload(image.getBytes(), params);

        return MapUtils.getString(uploadResult, "url");

    }

}
