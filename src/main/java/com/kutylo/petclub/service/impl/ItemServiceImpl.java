package com.kutylo.petclub.service.impl;

import com.kutylo.petclub.dto.request.ItemRequest;
import com.kutylo.petclub.dto.request.PaginationRequest;
import com.kutylo.petclub.dto.response.ItemResponse;
import com.kutylo.petclub.dto.response.PageResponse;
import com.kutylo.petclub.mapper.ItemMapper;
import com.kutylo.petclub.model.Item;
import com.kutylo.petclub.model.ItemCategory;
import com.kutylo.petclub.model.PetType;
import com.kutylo.petclub.repository.ItemRepository;
import com.kutylo.petclub.service.ItemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemMapper itemMapper;
    private final ItemRepository itemRepository;

    @Override
    public ItemResponse getItemById(int id) {
        log.info("Get item with id:{}", id);
        Item item = itemRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found item with id:" + id));
        return itemMapper.modelToResponse(item);
    }

    @Override
    public PageResponse<ItemResponse> getAllItems(PaginationRequest paginationRequest) {
        log.info("Get all items");
        Pageable pageable = paginationRequest.toPageable();
        Page<Item> page = itemRepository.findAll(pageable);

        PageResponse<ItemResponse> response = new PageResponse<ItemResponse>(
                page.getTotalPages(),
                page.getTotalElements(),
                page.map(itemMapper::modelToResponse)
                        .get()
                        .collect(Collectors.toList()));
        return response;
    }

    @Override
    public List<String> getAllItemCategory() {
        log.info("Get all item categories");
        return Arrays.stream(ItemCategory.values()).map(ItemCategory::getCategory).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllItemPetType() {
        log.info("Get all item petTypes");
        return Arrays.stream(PetType.values()).map(PetType::getName).collect(Collectors.toList());
    }

    @Override
    public PageResponse<ItemResponse> getAllItemsByCategory(String category, PaginationRequest paginationRequest) {
        log.info("Get all items by category:{}", category);
        Pageable pageable = paginationRequest.toPageable();
        Page<Item> page = itemRepository.findItemsByType(ItemCategory.valueOf(category.toUpperCase()), pageable);

        PageResponse<ItemResponse> response = new PageResponse<ItemResponse>(
                page.getTotalPages(),
                page.getTotalElements(),
                page.map(itemMapper::modelToResponse)
                        .get()
                        .collect(Collectors.toList()));
        return response;
    }

    @Override
    public PageResponse<ItemResponse> getAllItemsByPetType(String petType, PaginationRequest paginationRequest) {
        log.info("Get all items by pet type:{}", petType);
        Pageable pageable = paginationRequest.toPageable();
        Page<Item> page = itemRepository.findItemsByPetType(PetType.valueOf(petType.toUpperCase()), pageable);

        PageResponse<ItemResponse> response = new PageResponse<ItemResponse>(
                page.getTotalPages(),
                page.getTotalElements(),
                page.map(itemMapper::modelToResponse)
                        .get()
                        .collect(Collectors.toList()));
        return response;
    }

    @Override
    public ItemResponse save(ItemRequest itemRequest) {
        log.info("Save item:{}", itemRequest);
        Item item = itemMapper.requestToModel(itemRequest);
        item.setCreateDate(LocalDateTime.now());
        itemRepository.save(item);
        return itemMapper.modelToResponse(item);
    }

    @Override
    public ItemResponse update(int id, ItemRequest itemRequest) {
        log.info("Update item with id:{},{}", id, itemRequest);

        Item item = itemMapper.requestToModel(itemRequest);
        item.setId(id);
        item.setCreateDate(LocalDateTime.now());

        itemRepository.save(item);
        return itemMapper.modelToResponse(item);
    }

    @Override
    public void delete(int id) {
        log.info("Deleting item with id:{}", id);
        itemRepository.deleteById(id);
    }
}
