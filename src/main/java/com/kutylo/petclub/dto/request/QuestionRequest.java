package com.kutylo.petclub.dto.request;

import com.kutylo.petclub.dto.response.UserResponse;
import com.kutylo.petclub.model.CategoryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionRequest {

    private String title;
    private String text;
    private String imgUrl;
    private LocalDateTime createDate;
    private CategoryType category;
    private UserResponse user;
}
