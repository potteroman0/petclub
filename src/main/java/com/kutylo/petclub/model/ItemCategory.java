package com.kutylo.petclub.model;

public enum ItemCategory {
    FOOD("food"), TOYS("toys"), LITTER("litter");

    private String category;

    ItemCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
