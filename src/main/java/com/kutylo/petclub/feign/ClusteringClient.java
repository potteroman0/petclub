package com.kutylo.petclub.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "clusterAPI", url = "http://localhost:4567")
@Component
public interface ClusteringClient {

    @GetMapping("/{username}")
    public String getUserCluster(@PathVariable String username);
}
